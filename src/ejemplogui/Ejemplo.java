package ejemplogui;

import javax.swing.*;


public class Ejemplo {

    public Ejemplo() {
        JFrame frame = new JFrame("Hola mundo");
        JPanel contentPane = (JPanel) frame.getContentPane();
        JPanel panel = new JPanel();
        
        JLabel label = new JLabel("Etiqueta");
        JButton button = new JButton("Boton");
        panel.add(label);
        panel.add(button);
        contentPane.add(panel);
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 100);
        frame.setVisible(true);
        
    }
}
