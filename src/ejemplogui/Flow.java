
package ejemplogui;

import javax.swing.*;


public class Flow {
     public Flow() {
         JFrame frame = new JFrame("FlowLayout");
         JPanel contentPane = (JPanel) frame.getContentPane();
         JPanel panel = new JPanel();
         panel.add(new JButton("Botón 1"));
         panel.add(new JButton("Botón 2"));
         panel.add(new JButton("Botón 3"));
         panel.add(new JButton("Botón 4"));
         panel.add(new JButton("Botón 5"));
         contentPane.add(panel);
         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         frame.setSize(400, 200);
         frame.setVisible(true);
     }
}
