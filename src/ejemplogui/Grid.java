/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplogui;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author alberto
 */
public class Grid {
    
    public Grid() {
    JFrame frame = new JFrame("FlowLayout");
         JPanel contentPane = (JPanel) frame.getContentPane();
         JPanel panel = new JPanel();
         panel.setLayout(new GridLayout(3,2));
         panel.add(new JButton("Botón 1"));
         panel.add(new JButton("Botón 2"));
         panel.add(new JButton("Botón 3"));
         panel.add(new JButton("Botón 4"));
         panel.add(new JButton("Botón 5"));
         contentPane.add(panel);
         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         frame.setSize(400, 200);
         frame.setVisible(true);
    }
}
