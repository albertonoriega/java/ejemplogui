
package ejemplogui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class Ejercicio3 {
    public Ejercicio3() {
        // Creamos el frame
         JFrame frame = new JFrame("Mi segundo Layout");
         JPanel contentPane = (JPanel) frame.getContentPane();
         // Creamos el panel principal
         JPanel panelPrincipal = new JPanel();
         // En el panle principal creamos un BorderLayout
         panelPrincipal.setLayout(new BorderLayout());
         // Creamos un botón al norte
         panelPrincipal.add(new JButton("Botón 5"), BorderLayout.NORTH);
         // Creamos un botón al este
         panelPrincipal.add(new JButton("Botón 8"), BorderLayout.EAST);
         // Creamos un botón al sur
         panelPrincipal.add(new JButton("Botón 7"), BorderLayout.SOUTH);
         // Creamos un botón al oeste
         panelPrincipal.add(new JButton("Botón 6"), BorderLayout.WEST);
         // Creamos un panel secundario
         JPanel panelSecundario = new JPanel();
         // Al panel secundario le metemos un rejilla (Grid)
         panelSecundario.setLayout(new GridLayout(2, 2));
         // Hacemos un bucle para rellenar la rejilla de botones
         for (int i=0;i<4;i++){
             panelSecundario.add(new JButton("Botón "+i));
         }
        // Asignamos el panelSecundario al centro del layout del panelPrincipal 
         panelPrincipal.add( panelSecundario, BorderLayout.CENTER);
         // Añadimos el panelPrincipal 
         contentPane.add(panelPrincipal);
         // El frame se cerrará al cerrar la ventana
         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         // Dimensiones del frame
         frame.setSize(600, 300);
         // Hacemos visible el frame
         frame.setVisible(true);
    }
}
