
package ejemplogui;

import java.awt.BorderLayout;
import javax.swing.*;



public class Border {
    public Border(){
    JFrame frame = new JFrame("BorderLayout");
         JPanel contentPane = (JPanel) frame.getContentPane();
         JPanel panel = new JPanel();
         panel.setLayout(new BorderLayout());
         panel.add(new JButton("Botón 1"), BorderLayout.NORTH);
         panel.add(new JButton("Botón 2"), BorderLayout.EAST);
         panel.add(new JButton("Botón 3"), BorderLayout.SOUTH);
         panel.add(new JButton("Botón 4"), BorderLayout.WEST);
         panel.add(new JButton("Botón 5"), BorderLayout.CENTER);
         contentPane.add(panel);
         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         frame.setSize(400, 200);
         frame.setVisible(true);
    }
}
