
package ejemplogui;

import java.awt.GridLayout;
import javax.swing.*;



public class Ejercicio2 {
    public Ejercicio2() {
        JFrame frame = new JFrame("Mi primer Layout");
         JPanel contentPane = (JPanel) frame.getContentPane();
         JPanel panel = new JPanel();
         panel.setLayout(new GridLayout(5, 5));
         contentPane.add(panel);
         for (int i=0;i<25;i++){
             panel.add(new JButton("Botón "+i));
         }
         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         frame.setSize(600, 300);
         frame.setVisible(true);
    }
}
